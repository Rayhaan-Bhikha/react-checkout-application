import React from 'react'
import ProductsPage from './ProductsPage.jsx'

export default class HomePage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      productsPage: false
    }
    this.goToProducts = this.goToProducts.bind(this)
    this.homePage = this.homePage.bind(this)
  }

  goToProducts () {
    return this.setState({
      productsPage: true
    })
  }

  homePage () {
    return this.setState({
      productsPage: false
    })
  }

  render () {
    const HomePage = (
      <div className='jumbotron mt-5'>
        <h1 className='display-4'>Checkout Application!</h1>
        <hr className='my-4' />
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <p className='lead'>
          <button className='btn btn-info' id='productPageBtn' onClick={this.goToProducts}>Go to products</button>
        </p>
      </div>)
    const element = !this.state.productsPage ? HomePage : <ProductsPage homePage={this.homePage} />
    return (
      <div className='container'>
        {element}
      </div>
    )
  }
}
