import React from 'react'
import './style.scss'

export default class Basket extends React.Component {
  render () {
    var {items} = this.props
    var total = 0
    items.forEach(item => {
      total += Number(item.price.substring(1))
    })
    const checkoutBtn = items.length > 0 ? (<button onClick={this.props.showAlert} className='btn btn-dark' id='checkoutBtn'>Checkout</button>) : null
    return (
      <div className='row basket'>
        <div id='list' className='col-md-8'>
          <ul>
            {items.map((item, index) => <li key={index}>{item.name}</li>)}
          </ul>
        </div>
        <div className='col-md-4 d-flex flex-column '>
          <div className='p-2 bd-highlight'>Total: {total}</div>
          <div className='p-2 bd-highlight'>{checkoutBtn}
            <button className='btn btn-white ml-3' id='homePageBtn' onClick={this.props.homePage}>Home</button>
          </div>
        </div>
      </div>
    )
  }
}
