import React from 'react'
import Card from './Card.jsx'
import Basket from './Basket.jsx'
import {Modal, Button} from 'react-bootstrap'

export default class HomePage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      products: [],
      basketItems: [],
      showModal: false
    }
    this.addToBasket = this.addToBasket.bind(this)
    this.showAlert = this.showAlert.bind(this)
    this.closeAlert = this.closeAlert.bind(this)
  }

  componentDidMount () {
    fetch('http://localhost:8001/api/products')
      .then(response => {
        return response.json()
      })
      .then(data => {
        let products = data.map((product, index) => {
          return <Card item={product} index={index} addToBasket={this.addToBasket} key={index} />
        })
        this.setState({
          products: products
        })
      })
  }
  addToBasket (e) {
    e.preventDefault()
    let selectedItem = JSON.parse(e.target.getAttribute('product'))

    console.log(selectedItem)
    this.setState({
      basketItems: [...this.state.basketItems, selectedItem]
    })
  }

  showAlert () {
    return this.setState({
      showModal: true
    })
  }
  closeAlert () {
    return this.setState({
      showModal: false
    })
  }

  render () {
    const modal = (
      <div className='static-modal'>
        <Modal.Dialog>
          <Modal.Header>
            <Modal.Title>Items have been Purchased!</Modal.Title>
          </Modal.Header>
          <Modal.Footer>
            <Button onClick={this.closeAlert} bsStyle='success' id='closeAlertBtn'>Close</Button>
          </Modal.Footer>
        </Modal.Dialog>
      </div>
    )
    const alert = this.state.showModal ? modal : null
    return (
      <div className='container'>
        {this.state.products}
        <Basket {...this.props} items={this.state.basketItems} showAlert={this.showAlert} />
        {alert}
      </div>
    )
  }
}
