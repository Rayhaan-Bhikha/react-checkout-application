const express = require('express')
const app = express()
const cors = require('cors')
const path = require('path')

const port = process.env.PORT || 8001

app.use(cors())
app.use(express.static(path.resolve('dist')))

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '..', '..', 'dist', 'index.html'))
})

app.use('/api', require('./api'))

app.listen(port, () => {
  console.log(`server started on port: ${port}`)
})
